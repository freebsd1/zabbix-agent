#!/bin/sh

# Installing the zabbix agent 4.0.25

pkg add -f zabbix4-agent-4.0.25.txz

# Creating Zabbix Agent Configuration 

# Asking Zabbix Server IP

printf "\n"
read -p "Provide Zabbix Server IP: " srvip

echo "LogFile=/var/log/zabbix_agentd.log" >> /usr/local/etc/zabbix4/zabbix_agentd.conf
echo "Server=$srvip/24" >> /usr/local/etc/zabbix4/zabbix_agentd.conf
echo "ServerActive=$srvip" >> /usr/local/etc/zabbix4/zabbix_agentd.conf
echo "HostnameItem=system.hostname" >> /usr/local/etc/zabbix4/zabbix_agentd.conf
echo "Include=/usr/local/etc/zabbix4/zabbix_agentd.conf.d/*.conf" >> /usr/local/etc/zabbix4/zabbix_agentd.conf
echo "Timeout=20" >> /usr/local/etc/zabbix4/zabbix_agentd.conf

echo -e "Zabbix Agent Configuration file created successfully\n"

cp ./zfs.conf /usr/local/etc/zabbix4/zabbix_agentd.conf.d/
chmod +x /usr/local/etc/zabbix4/zabbix_agentd.conf.d/zfs.conf

cp -r ./scripts /usr/local/etc/zabbix4/
chmod +x /usr/local/etc/zabbix4/scripts/*.sh
chown -R zabbix:zabbix /usr/local/etc/zabbix4/

touch /var/log/zabbix_agentd.log
chown zabbix:zabbix /var/log/zabbix_agentd.log

# To start the agent service automatically when the machine reboot
sysrc zabbix_agentd_enable=YES

# Start the zabbix agent service
service zabbix_agentd start

exit 1