#!/bin/sh

pool_name=$1

status=$(zpool list -H -o health $pool_name)

if [ $status = 'ONLINE' ]; then
    echo 0
    exit 1

else if [ $status = 'DEGRADED' ]; then
    echo 1
    exit 1

else if [ $status = 'UNAVAIL' ]; then
    echo 2
    exit 1

fi
fi
fi


{ZFS on FreeBSD:zfs.zpool.health[{#POOLNAME}].str(ONLINE)}=0

{ZFS on FreeBSD:zfs.zpool.health[{#POOLNAME}].last(0)}>0