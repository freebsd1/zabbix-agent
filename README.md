# zabbix-agent

It has zabbix agent 4.0.25 installation files and configuration files with some sample zabbix templates

**Zabbix Agent Installation Steps**

Navigate to this cloned git repo directory and make the script 'zabbix_agent_install.sh' executable and install it by using the below commands

`# chmod +x zabbix_agent_install.sh`

`# ./zabbix_agent_install.sh`

During installation process, script will prompt to input the zabbix server IP for configuration and when it completes, can check the zabbix agent service status as below,

`# service zabbix_agentd status`

Import all the template files in zabbix web interface, add the host and link these templates to the added host.